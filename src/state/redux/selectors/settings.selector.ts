import { ISettings } from 'interfaces/store.types'
import get from 'lodash/get'

export const getDrawerToggle = (state: ISettings) => get(state, 'settings.drawer')
