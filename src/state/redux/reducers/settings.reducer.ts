import * as types from 'state/redux/constants'

const initialState = {
  drawer: false,
}

const settingsReducer = (
  state = initialState,
  action: { type: any; msg: any; err: any; data: any }
) => {
  switch (action.type) {
    case types.SET_DRAWER_TOGGLE:
      return {
        ...state,
        drawer: action.data,
      }

    default:
      return state
  }
}

export default settingsReducer
