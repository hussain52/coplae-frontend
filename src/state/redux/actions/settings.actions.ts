import * as types from 'state/redux/constants'

export const setDrawerToggle = (toggle: any) => async (dispatch: any) => {
  try {
    dispatch({ type: types.SET_DRAWER_TOGGLE, data: toggle })
  } catch (error) {
    console.error(error)
  }
}
