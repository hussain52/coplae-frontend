import * as types from 'state/redux/constants'

export const setUser = (user: any) => async (dispatch: any) => {
  try {
    dispatch({ type: types.SET_USER_DATA, data: user })
  } catch (error) {
    console.error(error)
  }
}
