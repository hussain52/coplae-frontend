import * as types from 'state/redux/constants'
import isEmpty from 'lodash/isEmpty'

export const setTemporaryUser = (user: any) => async (dispatch: any) => {
  try {
    if (!isEmpty(user)) {
      dispatch({ type: types.SET_TEMPORARY_USER, data: user })
    }
  } catch (error) {
    console.error(error)
  }
}
