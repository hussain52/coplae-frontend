import common from 'i18n/en/en.common'

const en = {
  ...common,
}

export default en
