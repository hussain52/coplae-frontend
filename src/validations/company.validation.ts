import * as yup from 'yup'

const companyValidation = yup.object().shape({
  name: yup.string().required(),
  size: yup.number().required(),
  userEmail: yup.string().email().required(),
  userFullName: yup.string().required(),
  domain: yup.string().required(),
  address: yup.string().required(),
  country: yup.string().required(),
})

export default companyValidation
