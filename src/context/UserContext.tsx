import React, { useContext, createContext, useState } from 'react'

const UserContext = createContext(null)

const UserContextProvider = ({ children }: { children: React.ReactNode }) => {
  const [darkMode, setDarkMode] = useState(true)

  return (
    <UserContext.Provider
      /* @ts-ignore */
      value={{
        darkMode,
        setDarkMode,
      }}
    >
      {children}
    </UserContext.Provider>
  )
}

export const useUserContext: any = () => useContext(UserContext)

export default UserContextProvider
