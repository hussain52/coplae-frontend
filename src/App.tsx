import React, { useEffect } from 'react'
import LayoutRouter from 'containers/Router'
import { useState } from 'react'
import 'styles/index.scss'
import 'styles/index.css'
import get from 'lodash/get'
import isEmpty from 'lodash/isEmpty'
import { useGet } from 'hooks/useApi'
import { setUser } from 'state/redux/actions/user.actions'
import { useDispatch, useSelector } from 'react-redux'
import { getAccessToken } from 'helpers'

const App = () => {
  const dispatch = useDispatch()

  const [loading, setLoading] = useState(true)

  const { response, refetch, isFetched } = useGet('admin/user', {
    retry: false,
    enabled: false,
  })

  const user = useSelector((state) => get(state, 'user.user'))
  const accessToken = getAccessToken()

  useEffect(() => {
    if (response?.success && response?.message === 'User information') {
      dispatch(setUser(response?.data))
      setLoading(false)
    } else {
      dispatch(setUser({}))
    }
  }, [response?.success])

  useEffect(() => {
    if (!isEmpty(accessToken)) {
      try {
        refetch()
      } catch (err) {
        setLoading(false)
        console.log('Token Error')
      }
    } else {
      setLoading(false)
    }
  }, [accessToken])

  return <LayoutRouter isUser={!isEmpty(user) && !loading && isFetched} loading={loading} />
}

export default App
