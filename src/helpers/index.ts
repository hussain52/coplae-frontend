import { parseCookies } from 'nookies'

export const getAccessToken = () => {
  const { accessToken } = parseCookies()
  return accessToken
}
