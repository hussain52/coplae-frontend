import isEmpty from 'lodash/isEmpty'
import { parseCookies } from 'nookies'

const baseUrl = process.env.REACT_APP_BASE_URL

/**
 *
 * API GET
 * Note: This hook is made to pass in the react-query. Try using it directly
 * @param query name of the api after base url. Check .env for baseurl
 * @returns fetch function
 */

const apiGet = async (query: string) => {
  const { accessToken } = parseCookies()

  const config: any = {
    method: 'GET',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  }

  if (!isEmpty(accessToken)) {
    config.headers = {
      Authorization: accessToken,
      ...config.headers,
    }
  }

  return fetch(`${baseUrl}/${query}`, config).then((res) => res.json())
}

export default apiGet
