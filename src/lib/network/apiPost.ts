import isEmpty from 'lodash/isEmpty'
import { parseCookies } from 'nookies'

const baseUrl = process.env.REACT_APP_BASE_URL

/**
 *
 * API POST
 * Note: This hook is made to pass in the react-query. Try using it directly
 * @param query name of the api after base url. Check .env for baseurl
 * @param body  pass the object of data to send to the server
 * @returns fetch function
 */

const apiPost = async (query: string, body: any) => {
  const { accessToken } = parseCookies()

  const config: any = {
    method: 'POST',
    body: JSON.stringify(body),
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  }

  if (!isEmpty(accessToken)) {
    config.headers = {
      Authorization: accessToken,
      ...config.headers,
    }
  }

  return fetch(`${baseUrl}/${query}`, config).then((response) => {
    return response.json()
  })
}

export default apiPost
