import includes from 'lodash/includes'

export const isDevHosted = includes(window.location.hostname, 'locahost')
export const isLocalHosted = includes(window.location.hostname, 'localhost')

export const isProdHosted = !isDevHosted && !isLocalHosted

export default { isProdHosted, isDevHosted, isLocalHosted }
