import { Route, Redirect } from 'react-router-dom'

/**
 * @isPublic - can public see this page without login?
 * @isUser - is user logged in?
 * @WhatItDoes - This component will return user to login page if they are not logged in.
 * and if they are logged in then it will just return the passed component
 */
const AuthRoute = ({
  isPublic = false,
  children,
  isUser,
  component: Component = null,
  exact,
  path,
  ...rest
}: {
  isPublic?: boolean
  isUser?: boolean
  children?: any
  component?: any
  exact?: boolean
  path: string
}) => {
  const onPublic = () => {
    if (isUser) {
      return <Redirect to="/dashboard" />
    } else {
      return Component ? Component : children
    }
  }
  const onPrivate = () => {
    if (isUser) {
      return Component ? Component : children
    } else {
      return <Redirect to="/login" />
    }
  }

  if (isPublic) {
    return (
      <Route {...rest} exact={exact} path={path} render={() => onPublic()} />
    )
  } else {
    return (
      <Route {...rest} exact={exact} path={path} render={() => onPrivate()} />
    )
  }
}

export default AuthRoute
