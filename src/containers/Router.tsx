import { lazy } from 'react'
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom'
import AuthRoute from 'containers/AuthRoute'
import AuthContainer from 'containers/AuthContainer'
import NotFound from 'pages/Common/NotFound'
import Login from 'pages/LoginRegister/Login'

const LoginPage = lazy(() => import('pages/LoginRegister/Login'))
const VerifyPage = lazy(() => import('pages/LoginRegister/Verify'))
const SetPassword = lazy(() => import('pages/LoginRegister/SetPassword'))
const ForgotPassword = lazy(() => import('pages/LoginRegister/ForgotPassword'))

interface IProps {
  loading: boolean
  isUser: boolean
}

const LayoutRouter = ({ loading, isUser }: IProps) => {
  return (
    <Router>
      <AuthContainer loading={loading}>
        <Switch>
          <AuthRoute isUser={isUser} component={<Login />} isPublic exact path="/login" />
          <AuthRoute isUser={isUser} component={<VerifyPage />} isPublic exact path="/verify" />
          <AuthRoute isUser={isUser} component={<SetPassword />} isPublic exact path="/set-password" />
          <AuthRoute isUser={isUser} component={<LoginPage />} isPublic exact path="/login" />
          <AuthRoute isUser={isUser} component={<ForgotPassword />} isPublic exact path="/forgot-password" />
          <AuthRoute isUser={isUser} component={<Redirect to="/dashboard" />} path="/" />

          {/* Error Page */}
          <Route component={NotFound} />
        </Switch>
      </AuthContainer>
    </Router>
  )
}

export default LayoutRouter
