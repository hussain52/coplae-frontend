import { FC } from 'react'

interface IProps {}

const Layout: FC<IProps> = ({ children, ...props }) => {
  return <div className="c-app c-default-layout">{children}</div>
}

export default Layout
