import { useTranslation } from 'react-i18next'

export function useLocale() {
  const { t } = useTranslation()
  return t
}
