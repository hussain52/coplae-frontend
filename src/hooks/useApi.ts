import { useMutation, useQuery } from 'react-query'
import apiGet from 'lib/network/apiGet'
import apiPost from 'lib/network/apiPost'
import toUpper from 'lodash/toUpper'

/**
 * Use this hook to fetch data from GET APIs.
 * @param apiName
 * @param queryParameters Custom config for react query  https://react-query.tanstack.com/overview
 * @returns repsonse fo the query and other react-query values. check bottom the file for all the values
 */

const useGet = (
  apiName: string,
  queryParameters: any = {
    staleTime: Infinity,
    retry: false,
    refetchOnWindowFocus: false,
  }
) => {
  const {
    data: response,
    error,
    ...rest
  } = useQuery([toUpper(apiName) + 'get'], async () => apiGet(apiName), queryParameters)

  return { response, error, ...rest }
}

/**
 * Use this hook to fetch data from POST APIs with form data.
 * @param apiName
 * @param queryParameters Custom config for react query  https://react-query.tanstack.com/overview
 * @returns repsonse and mutate function. Mostly used when we need to pass the data while calling the function. Foreg: on submit
 */

const useMutate = (
  apiName: string,
  queryParameters: any = {
    staleTime: Infinity,
    retry: false,
    refetchOnWindowFocus: false,
  }
) => {
  const {
    mutate,
    data: response,
    error,
    ...rest
  } = useMutation(async (body: any) => apiPost(apiName, body), queryParameters)

  return { response, error, mutate, ...rest }
}

/**
 * Use this hook to fetch data from POST APIs on inital page render
 * @param apiName
 * @param queryParameters Custom config for react query  https://react-query.tanstack.com/overview
 * @returns repsonse data and
 */

const usePost = (
  apiName: string,
  body?: any,
  queryParameters: any = {
    staleTime: Infinity,
    retry: false,
    refetchOnWindowFocus: false,
  }
) => {
  const {
    refetch,
    data: response,
    error,
    ...rest
  } = useQuery([toUpper(apiName) + 'post'], async () => apiPost(apiName, body), queryParameters)

  return { response, error, refetch, ...rest }
}

export { useGet, usePost, useMutate }

// Following values are returned in the query
// https://react-query.tanstack.com/reference/useQuery
/*
   const {
   data,
   dataUpdatedAt,
   error,
   errorUpdatedAt,
   failureCount,
   isError,
   isFetched,
   isFetchedAfterMount,
   isFetching,
   isIdle,
   isLoading,
   isLoadingError,
   isPlaceholderData,
   isPreviousData,
   isRefetchError,
   isStale,
   isSuccess,
   refetch,
   remove,
   status,
 }
*/
