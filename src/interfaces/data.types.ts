export interface CompanyProps {
  name: string
  size: string
  domain: string
  country: string
  address: string
  userEmail: string
  userFullName: string
}
