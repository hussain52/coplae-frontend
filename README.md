## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\

Open [http://localhost:3000](http://localhost:8000) to view it in the browser.

The page will reload if you make edits.\

You will also see any lint errors in the console.

### `yarn build`

Builds the app for production to the `build` folder.\

It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\

Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

## Directories

- `public` - Common Assets

- `locales` - Localization files in their respective languages

- `src/components` UI components (templates, atoms, molecules, organisms)

- `src/pages` pages that will be used by routers

- `src/containers` containers and higher-order-components (layout, sidebar, header, HOC, router config etc)

- `src/lib` - Pure libraries (not UI or redux related)

- `src/hooks` - Custom hooks

- `src/state` - Modular Redux stores, reducers.

## Atomic folder structure

**_Atoms:_**

Basic building blocks of matter, such as a button, input or a form label. They’re not useful on their own.

**_Molecules:_**

Grouping atoms together, such as combining a button, input and form label to build functionality.

**_Organisms:_**

Combining molecules together to form organisms that make up a distinct section of an interface (i.e. navigation bar)

**_Templates:_**

Consisting mostly of groups of organisms to form a page — where clients can see a final design in place.

## State management

Example of the state folder structure:

- /state
  - /redux
    - /actions
    - profile.actions.ts
    - /reducers
    - profile.reducers.ts

## Redux hooks

Use useDispatch and useSelectors for redux actions.

## Theming

TBD

## Avoid using

Try to avoid using large-sized libraries which doesn't support treeshaking like moment.
Always import functions using treeshaking.
